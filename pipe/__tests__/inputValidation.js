const pipe = require('../index')

describe('Validates input parameters', () => {
  const JIRA_BASE_URL = 'https://example.com'
  const JIRA_USER_EMAIL = 'dude@example.com'
  const JIRA_API_TOKEN = 'api_token'

  test('throws error if JIRA_API_TOKEN is not specified', async () => {
    try {
      await pipe({
        JIRA_BASE_URL,
        JIRA_USER_EMAIL: 'qwe',
        ISSUE: 'qwe',
        COMMENT: 'qweq',
      })
    } catch (error) {
      expect(error.message).toEqual('Please specify JIRA_API_TOKEN')
    }
  })

  test('throws error if JIRA_BASE_URL is not specified', async () => {
    try {
      await pipe({
        JIRA_API_TOKEN,
        JIRA_USER_EMAIL,
        ISSUE: 'qwe',
        COMMENT: 'qweq',
      })
    } catch (error) {
      expect(error.message).toEqual('Please specify JIRA_BASE_URL')
    }
  })

  test('throws error if JIRA_USER_EMAIL is not specified', async () => {
    try {
      await pipe({
        JIRA_BASE_URL,
        JIRA_API_TOKEN,
        ISSUE: 'qwe',
        COMMENT: 'qweq',
      })
    } catch (error) {
      expect(error.message).toEqual('Please specify JIRA_USER_EMAIL')
    }
  })

  test('throws error if COMMENT is not specified', async () => {
    try {
      await pipe({
        JIRA_BASE_URL,
        JIRA_API_TOKEN,
        JIRA_USER_EMAIL,
        ISSUE: 'qwe',
      })
    } catch (error) {
      expect(error.message).toEqual('Please specify COMMENT')
    }
  })

  test('throws error if ISSUE is not specified', async () => {
    try {
      await pipe({
        JIRA_BASE_URL,
        JIRA_API_TOKEN,
        JIRA_USER_EMAIL,
        COMMENT: 'qweq',
      })
    } catch (error) {
      expect(error.message).toEqual('Please specify ISSUE')
    }
  })
})
