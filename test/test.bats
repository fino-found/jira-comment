#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/jira-comment"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE} .
}

teardown() {
  run docker run \
        -e JIRA_BASE_URL=$JIRA_BASE_URL \
        -e JIRA_USER_EMAIL=$JIRA_USER_EMAIL \
        -e JIRA_API_TOKEN=$JIRA_API_TOKEN \
        -e ISSUE='BP-1' \
        -e COMMENT='Test comment' \
        -e BITBUCKET_WORKSPACE="$BITBUCKET_WORKSPACE" \
        -e BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
        -e BITBUCKET_BUILD_NUMBER="$BITBUCKET_BUILD_NUMBER" \
        -v $(pwd):$(pwd) \
        -v $(pwd)/tmp:/tmp \
        -w $(pwd) \
        ${DOCKER_IMAGE}
}

@test "Test comment is added successfully" {
    run docker run \
        -e JIRA_BASE_URL=$JIRA_BASE_URL \
        -e JIRA_USER_EMAIL=$JIRA_USER_EMAIL \
        -e JIRA_API_TOKEN=$JIRA_API_TOKEN \
        -e ISSUE='BP-1' \
        -e COMMENT='Test comment' \
        -e BITBUCKET_WORKSPACE="$BITBUCKET_WORKSPACE" \
        -e BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
        -e BITBUCKET_BUILD_NUMBER="$BITBUCKET_BUILD_NUMBER" \
        -v $(pwd):$(pwd) \
        -v $(pwd)/tmp:/tmp \
        -w $(pwd) \
        ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}


