FROM node:15-alpine

RUN apk update \
    && apk add --no-cache \
        ca-certificates=20191127-r2 \
        curl=7.67.0-r3
# hadolint ignore=DL3045
COPY pipe .

RUN npm i
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
