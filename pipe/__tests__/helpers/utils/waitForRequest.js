function waitForRequest (registerRequestListener, timeout = 2500) {
  const error = new Error(`Waiting > ${timeout} ms`)

  return new Promise((resolve, reject) => {
    const rejectTimeout = setTimeout(
      () => reject(error),
      timeout
    )

    function onReply (response) {
      clearTimeout(rejectTimeout)

      return (uri, body) => {
        resolve({ uri, body })

        return response
      }
    }

    registerRequestListener(onReply)
  })
}

module.exports = waitForRequest
