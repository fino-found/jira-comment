const _ = require('lodash')
const debug = require('debug')('pipe')

getBody = comment => {
  return {
    "body": {
      "type": "doc",
      "version": 1,
      "content": [
        {
          "type": "paragraph",
          "content": [
            {
              "text": comment,
              "type": "text"
            }
          ]
        }
      ]
    }
  };
}

module.exports = class {
  constructor ({ Jira }) {
    this.Jira = Jira
    this.context = {
      issue: {},
      myself: {},
    }
  }

  async execute (params) {
      return this.executeWrapped(params)
  }

  async executeWrapped ({ comment, issueKeyString }) {
    const myself = await this.Jira.getMyself()
      .catch((error) => {
        console.error('Cannot authenticate with Jira. Please check credentials')

        throw error
      })

    this.context.myself = myself

    const issue = await this.extractIssueFrom(issueKeyString)
      .catch((error) => {
        console.error('Failed to retrieve issue from Jira')

        throw error
      })

    if (!issue) {
      throw new Error('Cannot find issue key')
    }

    this.context.issue = issue

    await this.Jira
    .commentIssue(issue.key, getBody(comment))
    .catch((error) => {
      console.log('Failed to add comment')

      throw error
    })

    console.log(`Added a comment to issue ${issue.key}: ${comment}.`)
    console.log(`Link to issue: ${this.Jira.baseUrl}/browse/${issue.key}`)
  }

  async extractIssueFrom (extractString) {
    const issueIdRegEx = /([a-zA-Z0-9]+-[0-9]+)/g

    const match = extractString.match(issueIdRegEx)

    if (!match) {
      console.log(`String "${extractString}" does not contain issueKeys`)

      return
    }

    for (const issueKey of match) {
      debug(`searching for issue ${issueKey} ...`)
      const issue = await this.Jira.getIssue(issueKey)

      if (issue) {
        debug(`found issue ${issueKey}: ${JSON.stringify(issue, null, 4)}`)

        return issue
      }
    }
  }
}
